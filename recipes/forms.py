from django.forms import ModelForm
from recipes.models import Recipe, RecipeCategory, Ingredient, RecipeRating


class RecipeForm(ModelForm):
    class Meta:
        model = Recipe
        fields = ["name", "images", "description", "cooking_instructions", "total_servings"]

    def __init__(self, *args, **kwargs):
        initial_data = kwargs.get("initial", {})
        recipe = initial_data.get("recipe")

        super().__init__(*args, **kwargs)

        if recipe:
            for field_name in self.fields:
                if field_name in recipe.__dict__:
                    self.fields[field_name].initial = recipe.__dict__[field_name]


class RecipeCategoryForm(ModelForm):
    class Meta:
        model = RecipeCategory
        fields = [
            "for_breakfast",
            "for_lunch",
            "for_dinner",
            "for_snack",
        ]


class IngredientForm(ModelForm):
    class Meta:
        model = Ingredient
        fields = [
            "ingredient_name",
            "servings",
            "calories_per_serving",
            "protein_per_serving",
            "carbs_per_serving",
            "fat_per_serving",
        ]


class RecipeRatingForm(ModelForm):
    class Meta:
        model = RecipeRating
        fields = ["rating"]
