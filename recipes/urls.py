from django.urls import path
from . import views
from django.conf.urls.static import static
from django.conf import settings

# app_name = "recipes"
urlpatterns = [
    path("", views.latest_recipes, name="latest_recipes"),
    path("<str:category>", views.latest_recipes, name="meal_recipes"),
    path("recipes/add_recipe/", views.add_recipe, name="add_recipe"),
    path("recipes/<int:id>/add_ingredient", views.add_ingredient, name="add_ingredient"),
    path("recipes/<int:id>/edit_category", views.edit_category, name="edit_category"),
    path("recipes/<int:id>", views.single_recipe, name="recipe"),
    path("recipes/<int:id>/edit", views.edit_recipe, name="edit_recipe"),
    path("recipes/<int:id>/delete", views.delete_recipe, name="delete_recipe"),
    path("recipes/<int:recipe_id>/<int:ing_id>/delete_ingredient", views.delete_ingredient, name="delete_ingredient"),
    path("recipes/<int:recipe_id>/<int:ing_id>/edit", views.edit_ingredient, name="edit_ingredient"),
    path("recipes/ingredients/", views.all_ingredients, name="all_ingredients"),
    path("recipes/<int:id>/rating/", views.add_recipe_rating, name="add_recipe_rating"),
]
