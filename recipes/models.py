from django.db import models
from users.models import CustomUser
from django.core.validators import MaxValueValidator, MinValueValidator

# from PIL import Image


class Ingredient(models.Model):
    ingredient_name = models.CharField(max_length=256)
    servings = models.PositiveIntegerField()

    calories_per_serving = models.PositiveIntegerField(blank=True, null=True)
    protein_per_serving = models.PositiveIntegerField(blank=True, null=True)
    carbs_per_serving = models.PositiveIntegerField(blank=True, null=True)
    fat_per_serving = models.PositiveIntegerField(blank=True, null=True)

    def get_calories(self):
        return self.servings * self.calories_per_serving

    def get_macros_dict(self):
        return {
            "protein": self.protein_per_serving * self.servings,
            "carbohydrates": self.carbs_per_serving * self.servings,
            "fat": self.fat_per_serving * self.servings,
        }

    def __str__(self):
        return self.ingredient_name

    class Meta:
        verbose_name_plural = "Ingredients"
        verbose_name = "Ingredient"


class RecipeCategory(models.Model):
    for_breakfast = models.BooleanField()
    for_lunch = models.BooleanField()
    for_dinner = models.BooleanField()
    for_snack = models.BooleanField()

    class Meta:
        verbose_name_plural = "Recipe Categories"
        verbose_name = "Recipe Category"


class Recipe(models.Model):
    profile = models.ForeignKey(CustomUser, on_delete=models.CASCADE)

    name = models.CharField(max_length=50)
    images = models.ImageField(upload_to="images", blank=True)
    description = models.TextField(max_length=256)
    cooking_instructions = models.TextField(max_length=5000)
    total_servings = models.PositiveIntegerField()
    #  recipe_categories should probably be one to one?
    recipe_categories = models.ForeignKey(RecipeCategory, on_delete=models.CASCADE, related_name="recipes_categories")

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    ingredients = models.ManyToManyField(Ingredient)

    def get_average_rating(self):
        try:
            rating_list = []
            for rate in self.ratings.all():
                rating_list.append(rate.rating)
            return sum(rating_list) / len(rating_list)
        except ZeroDivisionError:
            return "no rating"

    def get_total_calories(self):
        total_calories = 0
        for ing in self.ingredients.all():
            total_calories += ing.get_calories()
        return total_calories

    def get_ingredients(self):
        return self.ingredients.objects.all()

    def get_categories(self):
        return self.recipe_categories.objects.all()

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Recipes"
        verbose_name = "Recipe"


class RecipeStep(models.Model):
    step_number = models.PositiveIntegerField()
    instruction = models.TextField()
    recipe = models.ForeignKey(Recipe, related_name="steps", on_delete=models.CASCADE)

    def recipe_title(self):
        return self.recipe.name

    class Meta:
        ordering = ["step_number"]
        verbose_name_plural = "Recipe Steps"
        verbose_name = "Recipe Step"


class RecipeRating(models.Model):
    RATING_CHOICES = (
        (1, 1),
        (2, 2),
        (3, 3),
        (4, 4),
        (5, 5),
    )
    rating = models.PositiveSmallIntegerField(
        default=5, choices=RATING_CHOICES, validators=[MinValueValidator(1), MaxValueValidator(5)]
    )
    recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE, related_name="ratings")

    class Meta:
        verbose_name_plural = "Recipe Ratings"
        verbose_name = "Recipe Rating"
