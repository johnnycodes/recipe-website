from django.shortcuts import render, get_object_or_404, redirect
from recipes.models import Recipe, Ingredient, RecipeCategory, RecipeRating
from recipes.forms import RecipeForm, RecipeCategoryForm, IngredientForm, RecipeRatingForm
from django.contrib.auth.decorators import login_required
from django.contrib import messages


def latest_recipes(request, category=None):
    recipes = Recipe.objects.all().order_by("-updated")
    if category == "breakfast":
        recipes = recipes.filter(recipe_categories__for_breakfast="True")
        meal = "breakfast"
    elif category == "lunch":
        recipes = recipes.filter(recipe_categories__for_lunch="True")
        meal = "lunch"
    elif category == "dinner":
        recipes = recipes.filter(recipe_categories__for_dinner="True")
        meal = "dinner"
    elif category == "snacks":
        recipes = recipes.filter(recipe_categories__for_snack="True")
        meal = "snacks"
    else:
        recipes = recipes[:4]
        meal = "latest_recipes"
    context = {
        "recipes": recipes,
        "meal": meal,
    }
    return render(request, "recipes/latest_recipes.html", context)


def single_recipe(request, id):
    recipe = get_object_or_404(Recipe, pk=id)
    rating_form = RecipeRatingForm
    context = {
        "recipe": recipe,
        "rating_form": rating_form,
    }
    return render(request, "recipes/recipe.html", context)


@login_required
def add_recipe(request):
    if request.method == "POST":
        recipe_form = RecipeForm(request.POST, request.FILES)
        cat_form = RecipeCategoryForm(request.POST)
        ing_form = IngredientForm(request.POST)
        if recipe_form.is_valid() and cat_form.is_valid() and ing_form.is_valid():
            save_rec_form = recipe_form.save(commit=False)
            save_cat_form = cat_form.save(commit=False)
            save_ing_form = ing_form.save(commit=False)
            save_rec_form.profile = request.user
            save_rec_form.recipe_categories = save_cat_form.id
            save_cat_form.save()
            save_ing_form.save()
            save_rec_form.recipe_categories = RecipeCategory.objects.get(id=save_cat_form.id)
            save_rec_form.save()
            save_rec_form.ingredients.add(save_ing_form.id)
            return redirect("recipe", id=save_rec_form.id)
    else:
        recipe_form = RecipeForm
        cat_form = RecipeCategoryForm
        ing_form = IngredientForm
    context = {"recipe_form": recipe_form, "cat_form": cat_form, "ing_form": ing_form}
    return render(request, "recipes/add_recipe.html", context)


@login_required
def edit_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    recipe_form = RecipeForm(instance=recipe)
    cat_form = RecipeCategoryForm(initial={"recipe": recipe})
    ing_form = IngredientForm(initial={"recipe": recipe})
    if request.method == "POST" and request.user == recipe.profile:
        recipe_form = RecipeForm(request.POST, request.FILES, instance=recipe)
        if recipe_form.is_valid():
            recipe_form.save()
            return redirect("recipe", id=id)
        else:
            messages.error(request, "this isn't your recipe")
            return redirect("recipe", id=id)

    context = {"recipe_form": recipe_form, "cat_form": cat_form, "ing_form": ing_form}
    return render(request, "recipes/edit_recipe.html", context)


@login_required
def delete_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    if request.method == "POST" and request.user == recipe.profile:
        recipe.delete()
    else:
        messages.error(request, "this isn't your recipe")
        return redirect("recipe", id=id)
    return redirect("latest_recipes")


@login_required
def delete_ingredient(request, recipe_id, ing_id):
    recipe = get_object_or_404(Recipe, id=recipe_id)
    ingredient = get_object_or_404(Ingredient, id=ing_id)
    if request.method == "POST" and request.user == recipe.profile:
        ingredient.delete()
    else:
        messages.error(request, "this isn't your recipe")
        return redirect("recipe", id=recipe_id)
    return redirect("recipe", id=recipe.id)


def add_recipe_rating(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    if request.method == "POST":
        form = RecipeRatingForm(request.POST)
        if form.is_valid():
            rating = form.save(commit=False)
            rating.recipe = recipe
            rating.save()
            messages.success(request, f"thanks for your rating of { rating.rating }/5 stars for { recipe.name}")
            return redirect("recipe", id=id)
    else:
        form = RecipeRatingForm()


@login_required
def add_ingredient(request, id):
    recipe = Recipe.objects.get(id=id)
    if request.method == "POST":
        form = IngredientForm(request.POST)
        if form.is_valid():
            p1 = form.save(commit=False)
            p1.save()
            recipe.ingredients.add(p1)
            messages.success(request, f"added { p1.ingredient_name } to { recipe.name}")
            return redirect("recipe", id=id)
        else:
            messages.error(request, "this isn't your recipe")
            return redirect("recipe", id=id)
    else:
        form = IngredientForm

    context = {"recipe": recipe, "form": form}
    return render(request, "recipes/add_ingredient.html", context)


@login_required
def edit_category(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    cat_form = RecipeCategoryForm(instance=recipe.recipe_categories)
    if request.method == "POST" and request.user == recipe.profile:
        cat_form = RecipeCategoryForm(request.POST, instance=recipe.recipe_categories)
        if cat_form.is_valid():
            cat_form.save()
            return redirect("recipe", id=id)
        else:
            messages.error(request, "this isn't your recipe")
            return redirect("recipe", id=id)
    context = {
        "recipe": recipe,
        "form": cat_form,
    }
    return render(request, "recipes/add_ingredient.html", context)


@login_required
def edit_ingredient(request, recipe_id, ing_id):
    recipe = get_object_or_404(Recipe, id=recipe_id)
    ing_form = IngredientForm(instance=recipe.ingredients.get(id=ing_id))
    if request.method == "POST" and request.user == recipe.profile:
        ing_form = IngredientForm(request.POST, instance=recipe.ingredients.get(id=ing_id))
        if ing_form.is_valid():
            ing_form.save()
            return redirect("recipe", id=recipe_id)
        else:
            messages.error(request, "this isn't your recipe")
            return redirect("recipe", id=recipe_id)
    context = {
        "recipe": recipe,
        "form": ing_form,
    }
    return render(request, "recipes/add_ingredient.html", context)


def all_ingredients(request):
    ingredients = Ingredient.objects.all()
    context = {
        "ingredients": ingredients,
    }
    return render(request, "recipes/ingredients.html", context)
