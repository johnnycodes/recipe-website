from django.shortcuts import render
from recipes.models import Recipe, Ingredient


def index_view(request):
    recipes = Recipe.objects.all()[:4]
    # recipes = Recipe.objects.latest('updated')[:4]
    ingredients = Ingredient.objects.all()
    context = {'recipes': recipes, 'ingredients': ingredients}
    return render(request, 'home/base.html', context)
