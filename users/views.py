from django.shortcuts import render, redirect
from django.contrib.auth import login
from .forms import NewUserForm
from django.contrib import messages

# Create your views here.


def register_request(request):
    if request.method == "POST":
        form = NewUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect('/')
        else:
            messages.error(request, "Passwords don't match. Try again!")
    form = NewUserForm()
    return render(
            request,
            template_name='registration/register.html',
            context={"register_form": form}
        )


def my_profile(request, id):
    pass